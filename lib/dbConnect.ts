/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 10:00:59 pm
 * ---------------------------------------------
 */

import * as mongoose from 'mongoose'
import constants from '@/utils/constants'

const MONGODB_URI = constants.MONGODB_URI

if (!MONGODB_URI) {
  throw new Error(
    'Please define the MONGODB_URI environment variable inside .env.local'
  )
}

let cached = global.mongoose

if (!cached) {
  cached = global.mongoose = { conn: null, promise: null }
}

async function dbConnect() {
  if (cached.conn) {
    return cached.conn
  }

  if (!cached.promise) {
    const opts: any = {
      autoIndex: constants.DB_AUTOINDEX,
    }

    cached.promise = mongoose.connect(MONGODB_URI, opts).then((mongoose) => {
      return mongoose
    })
  }
  try {
    cached.conn = await cached.promise
  } catch (e: any) {
    console.error({
      id: 'mongo-db-connection-error',
      message: e?.message,
    })
    throw e
  }
  return cached.conn
}

export default dbConnect
