/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 9:34:16 pm
 * ---------------------------------------------
 */

import mongoose from 'mongoose'
import bcrypt from 'bcrypt'
import { v4 as uuidv4 } from 'uuid'

const { Schema } = mongoose

const UserSchema = new Schema(
  {
    _id: {
      type: String,
      default: uuidv4,
    },
    firstName: String,
    lastName: String,
    email: String,
    username: {
      type: String,
      unique: true,
      lowercase: true,
    },
    password: String,
  },
  { timestamps: true, collection: 'Users' }
)

UserSchema.pre('save', async function (next) {
  try {
    if (!this.isModified('password')) {
      return next()
    }
    if (this?.password) {
      const hashedPassword = await bcrypt.hash(this.password, 8)
      this.password = hashedPassword
    }

    next()
  } catch (error: any) {
    return next(error)
  }
})

const User = mongoose.models.Users || mongoose.model('Users', UserSchema)

export default User
