/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 2:40:36 pm
 * ---------------------------------------------
 */

/** @returns {Promise<import('jest').Config>} */
module.exports = () => {
  return {
    verbose: true,
    // detectOpenHandles: true,
    // watch: true,
    silent: true,
    transform: {
      '\\.[jt]sx?$': ['babel-jest', { configFile: './babel.config.jest.js' }],
    },
    testTimeout: 50000,
    setupFilesAfterEnv: [
      './tests/setup.js',
      // can have more setup files here
    ],
    moduleNameMapper: {
      '^@/(.*)$': '<rootDir>/$1',
    },
  }
}

// module.exports = {
//   transform: {
//     '^.+\\.[t|j]sx?$': ['babel-jest', { configFile: './babel.config.jest.js' }],
//   },
// }
