/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 2:14:33 pm
 * ---------------------------------------------
 */

import dbConnect from './dbConnect'

let conn = null
let mongooseConnection = null

beforeAll(async () => {
  conn = await dbConnect()
  mongooseConnection = global.mongooseConnection = conn.mongoose.connection
})

afterAll(async () => {
  if (mongooseConnection) {
    // Get all collections
    const collections = await mongooseConnection.db.listCollections().toArray()

    // Create an array of collection names and drop each collection
    collections
      .map((collection) => collection.name)
      .forEach(async (collectionName) => {
        mongooseConnection.db.dropCollection(collectionName)
      })
    // Close the connection
    // await mongooseConnection.close()
    //   .then(() => {
    //   console.log('MongoDB connection closed')
    // })
  }
})
