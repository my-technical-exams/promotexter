/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 10:36:21 pm
 * ---------------------------------------------
 */

import constants from '@/utils/constants'
import mongoose from 'mongoose'

const MONGODB_URI = constants.MONGODB_URI_TEST

if (!MONGODB_URI) {
  throw new Error(
    'Please define the MONGODB_URI environment variable inside .env.local'
  )
}

let cached = global.mongooseTest

if (!cached) {
  cached = global.mongooseTest = { conn: null, promise: null }
}

async function dbConnect() {
  if (cached.conn) {
    return cached.conn
  }

  if (!cached.promise) {
    const opts: any = {
      autoIndex: false,
    }

    cached.promise = mongoose.connect(MONGODB_URI, opts).then((mongoose) => {
      return mongoose
    })
  }

  try {
    cached.conn = await cached.promise
  } catch (e: any) {
    console.error({
      id: 'mongo-db-connection-error',
      message: e?.message,
    })
    throw e
  }
  return cached.conn
}

export default dbConnect
