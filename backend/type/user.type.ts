/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 8:47:40 pm
 * ---------------------------------------------
 */

import { NextApiRequest, NextApiResponse } from 'next'

export interface IUserController {
  addUser: (
    req: NextApiRequest,
    res: NextApiResponse<any>
  ) => Promise<{ statusCode: number; data: any }>
  editUser: (
    req: NextApiRequest,
    res: NextApiResponse<any>
  ) => Promise<{ statusCode: number; data: any }>
  deleteUser: (
    req: NextApiRequest,
    res: NextApiResponse<any>
  ) => Promise<{ statusCode: number; data: any }>
  getUser: (
    req: NextApiRequest,
    res: NextApiResponse<any>
  ) => Promise<{ statusCode: number; data: any }>
  getUsers: (
    req: NextApiRequest,
    res: NextApiResponse<any>
  ) => Promise<{ statusCode: number; data: any }>
}
