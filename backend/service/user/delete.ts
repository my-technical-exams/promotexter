/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 9:00:33 pm
 * ---------------------------------------------
 */

import { NotFoundException } from '@/backend/exceptions/data'
import User from '@/models/User'

async function deleteUser(userId: string) {
  try {
    const exists = await User.exists({ _id: userId }).lean()
    if (!exists) throw new NotFoundException('User Not Found')

    const deletedUser = await User.deleteOne({ _id: userId })

    return deletedUser
  } catch (err) {
    console.log({
      id: 'userService-delete-error',
      err,
      params: {
        userId,
      },
    })
    throw err
  }
}

export default deleteUser
