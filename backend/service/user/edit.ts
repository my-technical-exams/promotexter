/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 10:00:18 pm
 * ---------------------------------------------
 */
import bcrypt from 'bcrypt'
import {
  NotFoundException,
  UnprocessableEntity,
} from '@/backend/exceptions/data'
import User from '@/models/User'
import { IUserParams } from './add'

interface IEditUserParams extends IUserParams {
  userId: string
}

async function edit(params: IEditUserParams) {
  try {
    const { username, userId, password } = params

    const user: any = await User.findById({ _id: userId }).lean()
    if (!user) throw new NotFoundException('User Not Found')

    const exists = await User.exists({
      username,
      _id: { $ne: userId },
    }).lean()

    if (exists) throw new UnprocessableEntity('username already exists')

    if (password?.length) {
      const hashedPassword = await bcrypt.hash(password, 8)
      params.password = hashedPassword
    }

    const updatedUser = await User.findByIdAndUpdate(userId, params, {
      new: true,
      upsert: false,
    })

    return updatedUser
  } catch (err) {
    console.log({
      id: 'userService-edit-error',
      err,
      params,
    })
    throw err
  }
}

export default edit
