/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 8:17:44 pm
 * ---------------------------------------------
 */

import { UnprocessableEntity } from '@/backend/exceptions/data'
import User from '@/models/User'

export interface IUserParams {
  firstName: string
  lastName: string
  email: string
  username: string
  password: string
}
async function add(params: IUserParams) {
  try {
    const exists = await User.exists({ username: params.username }).lean()
    if (exists) throw new UnprocessableEntity('username already exists')

    const newUser = await User.create(params)
    return newUser
  } catch (err) {
    console.log({
      id: 'userService-add-error',
      err,
      params,
    })
    throw err
  }
}

export default add
