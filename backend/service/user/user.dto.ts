/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 10:35:23 pm
 * ---------------------------------------------
 */

import Joi from 'joi'

const addUserSchema = Joi.object({
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  email: Joi.string().email(),
  username: Joi.string().required(),
  password: Joi.string().required(),
})

const getUserSchema = Joi.object({
  userIds: Joi.array().items(Joi.string().required()),
})

const querySchema = Joi.object({
  id: Joi.string().required(),
})

export { addUserSchema, getUserSchema, querySchema }
