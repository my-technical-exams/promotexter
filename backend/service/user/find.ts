/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 10:15:09 pm
 * ---------------------------------------------
 */

import { NotFoundException } from '@/backend/exceptions/data'
import User from '@/models/User'

interface IFindUsersParams {
  userId: string[] | string
}

async function find(params?: IFindUsersParams) {
  try {
    if (params?.userId && typeof params?.userId === 'string') {
      const user = await User.findById(
        { _id: params.userId },
        { password: false }
      ).lean()
      if (!user) throw new NotFoundException('User Not Found')
      return user
    }
    const $match: any = {}
    if (params?.userId?.length) {
      $match['_id'] = { $in: params.userId }
    }

    const users = await User.find($match, { password: false }).lean()
    return users
  } catch (err) {
    console.log({
      id: 'userService-find-error',
      err,
      params,
    })
    throw err
  }
}

export default find
