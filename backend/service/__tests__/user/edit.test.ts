/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 9:42:44 pm
 * ---------------------------------------------
 */

import add from '../../user/add'
import edit from '../../user/edit'

describe('Edit User Test', () => {
  let body = {
    firstName: 'geraldine',
    lastName: 'amen',
    email: 'gdine.amen@gmail.com',
    username: 'gdine',
    password: '!Password123',
  }

  let createdUser: any = null

  test('should edit user', async () => {
    const user = await add(body)
    createdUser = user

    body.email = 'gdine.amen2@gmail.com'
    const editedUser = await edit({ ...body, userId: createdUser._id })

    expect(editedUser._id).toBe(createdUser._id)
    expect(editedUser?.email).toBe(body.email)
    expect(editedUser.password).not.toBe(body.password)
  })

  test('should return error for duplicate username', async () => {
    body.username = 'gdineNew'
    await add(body)

    expect(async () => {
      await edit({ ...body, userId: createdUser._id })
    }).rejects.toThrow('username already exists')
  })

  test('should be able to edit username', async () => {
    body.username = 'gdine.new.without.duplicate'

    const updatedUser = await edit({ ...body, userId: createdUser._id })
    expect(updatedUser.username).toBe(body.username)
  })

  test(`should throw "User Not Found" error`, async () => {
    expect(async () => {
      await edit({ ...body, userId: 'invalidId' })
    }).rejects.toThrow('User Not Found')
  })
})

export {}
