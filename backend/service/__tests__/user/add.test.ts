/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 10:18:03 pm
 * ---------------------------------------------
 */

import add from '../../user/add'

describe('Add User Test', () => {
  test('should insert in database', async () => {
    const body = {
      firstName: 'geraldine',
      lastName: 'amen',
      email: 'gdine.amen@gmail.com',
      username: 'gdine',
      password: '!Password123',
    }
    const user = await add(body)
    expect(user?.firstName).toBe(body.firstName)
    expect(user.password).not.toBe(body.password)
    expect(user._id).not.toBeUndefined()
  })

  test('should return error for same username', async () => {
    const body = {
      firstName: 'geraldine',
      lastName: 'amen',
      email: 'gdine.amen@gmail.com',
      username: 'gdine',
      password: '!Password123',
    }

    expect(async () => {
      const user = await add(body)
    }).rejects.toThrow('username already exists')
  })
})

export {}
