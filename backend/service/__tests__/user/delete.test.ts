/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 10:12:27 pm
 * ---------------------------------------------
 */

import add from '../../user/add'
import deleteUser from '../../user/delete'
import find from '../../user/find'

describe('Delete User Test', () => {
  test('should delete user in database', async () => {
    const body = {
      firstName: 'geraldine',
      lastName: 'amen',
      email: 'gdine.amen@gmail.com',
      username: 'gdine',
      password: '!Password123',
    }
    const user = await add(body)

    await deleteUser(user._id)

    expect(async () => {
      await find({ userId: user._id })
    }).rejects.toThrow('User Not Found')
  })

  test(`should throw "User Not Found" error for invalid userId`, async () => {
    expect(async () => {
      await deleteUser('invalidId')
    }).rejects.toThrow('User Not Found')
  })
})

export {}
