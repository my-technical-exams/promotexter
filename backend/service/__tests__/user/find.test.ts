/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 10:34:52 pm
 * ---------------------------------------------
 */

import add from '../../user/add'
import find from '../../user/find'

describe('Find User Test', () => {
  test('should return user details except password', async () => {
    const body = {
      firstName: 'geraldine',
      lastName: 'amen',
      email: 'gdine.amen@gmail.com',
      username: 'gdine',
      password: '!Password123',
    }
    const createdUser = await add(body)

    const user: any = await find({ userId: createdUser._id })
    expect(user?.password).toBeUndefined()
    expect(user.email).toBe(body.email)
    expect(user._id).not.toBeUndefined()
  })
})

export {}
