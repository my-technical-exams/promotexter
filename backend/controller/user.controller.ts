/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 8:59:45 pm
 * ---------------------------------------------
 */

import { NextApiRequest, NextApiResponse } from 'next'
import add from '../service/user/add'
import deleteUserService from '../service/user/delete'
import edit from '../service/user/edit'
import find from '../service/user/find'
import { IUserController } from '../type/user.type'

const userController = {} as IUserController

userController.addUser = async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const params = req.body
    const data = await add(params)
    return { statusCode: 201, data }
  } catch (err) {
    console.log({
      id: 'userController-addUser-error',
      err,
      params: {
        req,
      },
    })
    throw err
  }
}

userController.editUser = async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const params = req.body
    const userId = req.query.id

    const data = await edit({ ...params, userId })
    return { statusCode: 200, data }
  } catch (err) {
    console.log({
      id: 'userController-edit-error',
      err,
      params: {
        req,
      },
    })
    throw err
  }
}

userController.deleteUser = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  try {
    const { id } = req.query
    const data = await deleteUserService(id as string)
    return { statusCode: 200, data }
  } catch (err) {
    console.log({
      id: 'userController-delete-error',
      err,
      params: {
        req,
      },
    })
    throw err
  }
}

userController.getUser = async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const { id } = req.query
    const data = await find({ userId: id as string })
    return { statusCode: 200, data }
  } catch (err) {
    console.log({
      id: 'userController-getUser-error',
      err,
      params: {
        req,
      },
    })
    throw err
  }
}

userController.getUsers = async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const params = req.body
    const data = await find(params)
    return { statusCode: 200, data }
  } catch (err) {
    console.log({
      id: 'userController-getUsers-error',
      err,
      params: {
        req,
      },
    })
    throw err
  }
}

export { userController }
