/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 10:47:51 am
 * ---------------------------------------------
 */

class NotFoundException extends Error {
  response: string
  constructor(response: string) {
    super(response)
    this.response = response
  }
}

class DataValidationError extends Error {
  response: string

  constructor(response: string) {
    super(response)
    this.response = response
  }
}

class UnprocessableEntity extends Error {
  response: any
  constructor(response: any) {
    super(response)
    this.response = response
  }
}

class Forbidden extends Error {
  response: any
  constructor(response: any) {
    super(response)
    this.response = response
  }
}

export {
  NotFoundException,
  DataValidationError,
  UnprocessableEntity,
  Forbidden,
}
