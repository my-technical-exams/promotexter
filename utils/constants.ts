/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 10:36:39 pm
 * ---------------------------------------------
 */

const constants = {
  MONGODB_URI: <string>process.env.MONGODB_URI,
  MONGODB_URI_TEST: <string>process.env.MONGODB_URI_TEST,
  DB_AUTOINDEX: <string>process.env.DB_AUTOINDEX || false,
}

export default constants
