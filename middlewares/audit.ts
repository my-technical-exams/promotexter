/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 1:07:19 pm
 * ---------------------------------------------
 */

import { randomUUID } from 'crypto'
import { NextApiRequest, NextApiResponse } from 'next'

const auditMiddleWare = async (
  req: NextApiRequest,
  res: NextApiResponse,
  next: any
) => {
  global.logId = randomUUID()
  console.log({
    id: 'api-audit-request',
    logId,
    data: {
      headers: req.headers,
      body: req.body,
      path: req.url,
      method: req.method,
      query: req.query,
      cookies: req.cookies,
    },
  })

  res.on('close', function () {
    console.log({
      id: 'api-audit-response',
      logId,
      data: {
        headers: res.getHeaders(),
        status: res.statusCode,
        path: req.url,
        method: req.method,
        cookies: res.statusMessage,
      },
    })
  })

  next()
}

export default auditMiddleWare
