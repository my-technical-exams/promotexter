/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 2:05:41 pm
 * ---------------------------------------------
 */

import { NextApiRequest, NextApiResponse } from 'next'

import {
  DataValidationError,
  Forbidden,
  NotFoundException,
  UnprocessableEntity,
} from '../backend/exceptions/data'

const APIHandlerWrapper =
  (handler: (req: NextApiRequest, res: NextApiResponse) => any) =>
  async (req: NextApiRequest, res: NextApiResponse) => {
    try {
      console.log({
        id: 'handler-wrapper-handler-call',
        logId: global.logId,
        httpMethod: req.method,
        url: req.url,
        body: req.body,
        query: req.query,
        headers: req.headers,
      })

      const result = await handler(req, res) // Invoke the handler

      return res.status(result?.status || 200).json({
        statusCode: result?.status || 200,
        success: true,
        data: result?.data,
      })
    } catch (error: any) {
      console.error({
        id: 'handler-wrapper-error',
        logId: global.logId,
        data: {
          message: JSON.stringify(error.response),
          path: req.url,
          payload: req.body,
          parsedError: JSON.stringify(error, [
            'message',
            'arguments',
            'type',
            'name',
            'stack',
            'operation',
            'index',
            'documentId',
            'payload',
            'clientResponse',
          ]),
        },
      })

      const success = false
      if (error instanceof NotFoundException) {
        return res.status(404).json({
          statusCode: 404,
          success,
          message: error.response,
        })
      }

      if (error instanceof DataValidationError) {
        return res.status(400).json({
          statusCode: 400,
          success,
          message: error.response,
        })
      }

      if (error instanceof UnprocessableEntity) {
        return res.status(422).json({
          statusCode: 422,
          success,
          message: error.response,
        })
      }

      if (error instanceof Forbidden) {
        return res.status(403).json({
          statusCode: 403,
          success,
          message: error.response,
        })
      }

      return res.status(500).json({
        statusCode: 500,
        success,
        message: 'Internal Server Error',
      })
    }
  }

export default APIHandlerWrapper
