/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 10:55:03 am
 * ---------------------------------------------
 */

import withJoi from 'next-joi'

const validateWithJoi = withJoi({
  onValidationError: (req, res, error) => {
    console.error({
      id: 'data-schema-validation-error',
      data: {
        message: error.message,
        payload: JSON.stringify(req.body),
        path: req.url,
        parsedError: JSON.stringify(error, [
          'message',
          'arguments',
          'type',
          'name',
          'stack',
          'operation',
          'index',
          'documentId',
          'payload',
          'clientResponse',
        ]),
      },
    })

    return res.status(400).json({
      statusCode: 400,
      success: false,
      message: error.message,
    })
  },
})

export default validateWithJoi
