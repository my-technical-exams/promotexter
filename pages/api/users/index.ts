/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 8:34:59 pm
 * ---------------------------------------------
 */

import { NextApiRequest, NextApiResponse } from 'next'
import { createRouter } from 'next-connect'

import auditMiddleWare from '@/middlewares/audit'
import validateWithJoi from '@/middlewares/validateWithJoi'
import { addUserSchema, getUserSchema } from '@/backend/service/user/user.dto'
import APIHandlerWrapper from '@/middlewares/APIHandlerWrapper'
import { userController } from '@/backend/controller/user.controller'
import dbConnect from '@/lib/dbConnect'

const router = createRouter<NextApiRequest, NextApiResponse>()

router
  .use(auditMiddleWare)
  .use(async (req, res, next) => {
    await dbConnect()
    next()
  })
  .post(
    validateWithJoi({ body: addUserSchema }),
    APIHandlerWrapper(userController.addUser)
  )
  .get(
    validateWithJoi({ body: getUserSchema }),
    APIHandlerWrapper(userController.getUsers)
  )

export default router.handler({
  onNoMatch: (req: NextApiRequest, res: NextApiResponse) => {
    res.status(404).json({
      statusCode: 404,
      success: false,
      message: 'API Not Found',
    })
  },
})
