/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 8:57:54 pm
 * ---------------------------------------------
 */

import { NextApiRequest, NextApiResponse } from 'next'
import { createRouter } from 'next-connect'

import auditMiddleWare from '@/middlewares/audit'
import APIHandlerWrapper from '@/middlewares/APIHandlerWrapper'
import { userController } from '@/backend/controller/user.controller'
import validateWithJoi from '@/middlewares/validateWithJoi'
import dbConnect from '@/lib/dbConnect'
import { addUserSchema, querySchema } from '@/backend/service/user/user.dto'

const router = createRouter<NextApiRequest, NextApiResponse>()

router
  .use(auditMiddleWare)
  .use(async (req, res, next) => {
    await dbConnect()
    next()
  })
  .get(
    validateWithJoi({ query: querySchema }),
    APIHandlerWrapper(userController.getUser)
  )
  .patch(
    validateWithJoi({ query: querySchema, body: addUserSchema }),
    APIHandlerWrapper(userController.editUser)
  )
  .delete(APIHandlerWrapper(userController.deleteUser))

export default router.handler({
  onNoMatch: (req: NextApiRequest, res: NextApiResponse) => {
    res.status(404).json({
      statusCode: 404,
      success: false,
      message: 'API Not Found',
    })
  },
})
