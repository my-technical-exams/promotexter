/*
 * ---------------------------------------------
 * Author: Geraldine
 * Date:   Thursday February 1st 2024
 * Last Modified by: Geraldine - <gdine.amen@gmail.com>
 * Last Modified time: February 1st 2024, 3:34:45 pm
 * ---------------------------------------------
 */

import { NextApiRequest, NextApiResponse } from 'next'
import { createRouter } from 'next-connect'

import auditMiddleWare from '@/middlewares/audit'

import APIHandlerWrapper from '@/middlewares/APIHandlerWrapper'

export const phoneLettersCombinations = (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  try {
    const { input } = req.body

    const numberWithLetters: { [key: string]: string } = {
      '1': '',
      '2': 'abc',
      '3': 'def',
      '4': 'ghi',
      '5': 'jkl',
      '6': 'mno',
      '7': 'pqrs',
      '8': 'tuv',
      '9': 'wxyz',
    }

    if (!input || !input?.length) return { data: [] }

    const combinations: string[] = []

    const bfs = (index: number, string: string) => {
      if (index === input.length) {
        combinations.push(string)
      } else {
        let lettersOfNumber = numberWithLetters[input[index]]
        for (let i = 0; i < lettersOfNumber.length; i++) {
          bfs(index + 1, string + lettersOfNumber[i])
        }
      }
    }

    bfs(0, '')

    return { data: combinations }
  } catch (error: any) {
    console.log({
      id: 'phoneLettersCombinations-error',
      error,
      params: {
        ...req.body,
      },
    })
    throw error
  }
}

const router = createRouter<NextApiRequest, NextApiResponse>()

router.use(auditMiddleWare).post(APIHandlerWrapper(phoneLettersCombinations))

export default router.handler({
  onNoMatch: (req: NextApiRequest, res: NextApiResponse) => {
    res.status(404).json({
      statusCode: 404,
      success: false,
      message: 'API Not Found',
    })
  },
})
