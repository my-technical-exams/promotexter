This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Setup Backend

1. Prepare the .env. Run `cp .env.example .env`
2. Download docker desktop [https://www.docker.com/products/docker-desktop]
3. Run `docker-compose up` install database
4. On a separate terminal, run `yarn install` to install dependencies.
5. Create a database and name it `promotexter`. Set up the authorization of database.
6. Run `yarn dev ` to run the App
